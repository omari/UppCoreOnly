UPPOUT = _out/
CINC = -I./ -I/usr/include/freetype2 -I/usr/include/gtk-2.0 -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -I/usr/lib/gtk-2.0/include -I/usr/include/cairo -I/usr/include/pango-1.0 -I/usr/include/atk-1.0 -I/usr/X11R6/include -I/usr/X11R6/include/freetype2 -I/usr/X11R6/include/gtk-2.0 -I/usr/X11R6/include/glib-2.0 -I/usr/X11R6/lib/glib-2.0/include -I/usr/X11R6/lib/gtk-2.0/include -I/usr/X11R6/include/cairo -I/usr/X11R6/include/pango-1.0 -I/usr/X11R6/include/atk-1.0 -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/gtkglext-1.0 -I/usr/lib/gtkglext-1.0/include -I/usr/lib/i386-linux-gnu/glib-2.0/include -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/lib/i386-linux-gnu/gtk-2.0/include -I/usr/lib/x86_64-linux-gnu/gtk-2.0/include
Macro =  -DflagGCC -DflagSHARED -DflagLINUX -DflagPOSIX -DflagMT
CXX = c++
LINKER = $(CXX)

CFLAGS = -O3 -ffunction-sections -fdata-sections  
CXXFLAGS = -O3 -ffunction-sections -fdata-sections  -std=c++14

LDFLAGS = -Wl,--gc-sections -lz -lpthread -ldl -lrt
LIBPATH = -L"/usr/X11R6/lib" -L"/usr/lib"
AR = ar -sr

OutDir_core_only = $(UPPOUT)core_only/Debug-Debug_Full-Gcc-Main/
Macro_core_only = $(Macro) -DflagMAIN
SRC_core_only=$(wildcard core_only/*.cpp core_only/*.c )

OBJ_core_only_=$(SRC_core_only:.cpp=.o)
OBJ_core_only = $(addprefix $(OutDir_core_only), $(OBJ_core_only_))

OutDir_Core = $(UPPOUT)Core/Debug-Debug_Full-Gcc/
Macro_Core = $(Macro)
SRC_Core=$(wildcard Core/*.cpp Core/*.c )
SRC_Core_Lib=$(wildcard Core/lib/*.c )

OBJ_Core__=$(SRC_Core:.cpp=.o)
OBJ_Core_=$(notdir  $(OBJ_Core__))
OBJ_Core = $(addprefix $(OutDir_Core), $(OBJ_Core_))

OBJ_Core_Lib=$(OutDir_Core)lz4.o $(OutDir_Core)xxhash.o

OutDir = $(OutDir_core_only)
OutFile = core_only.out

.PHONY: all
all: prepare $(OutFile)

.PHONY: build_info
build_info:
	date '+#define bmYEAR    %y%n'\
	'#define bmMONTH   %-m%n'\
	'#define bmDAY     %-d%n'\
	'#define bmHOUR    %-H%n'\
	'#define bmMINUTE  %-M%n'\
	'#define bmSECOND  %-S%n'\
	'#define bmTIME    Time(%y, %-m, %-d, %-H, %-M, %-S)' > build_info.h
	echo '#define bmMACHINE "'`hostname`'"' >> build_info.h
	echo '#define bmUSER    "'`whoami`'"' >> build_info.h

.PHONY: prepare
prepare: build_info
	-mkdir -p $(OutDir)
	-mkdir -p $(OutDir_core_only)
	-mkdir -p $(OutDir_Core)

$(OutFile):  \
	$(OutDir_core_only)core_only.o \
	$(OBJ_Core_Lib) \
	$(OutDir_Core)Core.a
	$(LINKER) -static -o $(OutFile) -ggdb $(LIBPATH) -Wl,-O,2 -Wl,--start-group  \
		$(OutDir_core_only)core_only.o \
			$(OutDir_Core)Core.a \
			-Wl,--end-group $(LDFLAGS)

CORE_HEADERS=$(wildcard Core/*.h Core/*.hpp Core/*.i Core/*.icpp Core/lib/*.h ) plugin/z/lib/zconf.h plugin/z/lib/zlib.h uppconfig.h

$(OutDir_core_only)core_only.o: core_only/core_only.cpp $(CORE_HEADERS)
	$(CXX) -c -x c++ $(CXXFLAGS) $(CINC) $(Macro_core_only)  core_only/core_only.cpp -o $(OutDir_core_only)core_only.o
	
$(OutDir_Core)%.o: Core/%.cpp  $(CORE_HEADERS)
	$(CXX) -c -x c++ $(CXXFLAGS) $(CINC) $(Macro_Core)  $< -o $@

$(OutDir_Core)%.o: Core/lib/%.c  $(CORE_HEADERS)
	$(CXX) -c -x c $(CFLAGS) $(CINC) $(Macro_Core)  $< -o $@

$(OutDir_Core)Core.a: $(OBJ_Core) $(OBJ_Core_Lib)
	$(AR) $(OutDir_Core)Core.a $(OBJ_Core) $(OBJ_Core_Lib)

.PHONY: clean
clean:
	if [ -d $(UPPOUT) ]; then rm -rf $(UPPOUT); fi;
